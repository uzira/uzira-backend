from django.urls import path
from . import views

urlpatterns = [
    path('', views.TeamList.as_view()),
    path('<int:pk>/', views.TeamDetail.as_view()),
    path('<int:pk>/accounts/', views.TeamAccounts.as_view()),
    path('<int:pk>/accounts/add/', views.TeamAccountsAdd.as_view()),
    path('<int:pk>/accounts/delete/', views.TeamAccountsDelete.as_view()),
]