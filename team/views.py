from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied, NotFound
from django.core.exceptions import ObjectDoesNotExist

from user.serializers import UserEmailSerializer
from user.models import User
from .models import Team
from .serializers import TeamSerializer, TeamAccountsSerializer

class TeamList(APIView):
    """
    List all teams the user belongs to, or create a new team with the user being
    the admin.
    """
    def get_permissions(self):
        return IsAuthenticated(),

    def get(self, request, format=None):
        """
        Returns a list of teams the user belongs to.
        """
        teams = Team.objects.filter(accounts__id=request.user.id)
        serializer = TeamSerializer(teams, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TeamSerializer(data=request.data)
        if serializer.is_valid():
            team = serializer.save()
            team.accounts.add(request.user)
            team.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamAccounts(APIView):
    """
    Returns a list of given team's accounts.
    """
    def get_permissions(self):
        return IsAuthenticated(),

    def get(self, request, pk, format=None):
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")
        if request.user in team.accounts.all():
            serializer = TeamAccountsSerializer(team)
            return Response(serializer.data)
        raise PermissionDenied()

class TeamAccountsAdd(APIView):
    """
    Adds the user from the request body to the team.
    """
    def get_permissions(self):
        return IsAuthenticated(),

    def post(self, request, pk, format=None):
        serializer = UserEmailSerializer(data=request.data)
        if serializer.is_valid():
            try:
                team = Team.objects.get(pk=pk)
            except ObjectDoesNotExist:
                raise NotFound("Team not found.")
            if request.user in team.accounts.all():
                try:
                    user = User.objects.get(email=serializer.validated_data['email'])
                except ObjectDoesNotExist:
                    raise NotFound("User not found.")
                team.accounts.add(user)
                return Response({"message": "User added successfully."})
            raise PermissionDenied()
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TeamAccountsDelete(APIView):
    """
    Deletes the user from the request body from the team.
    """
    def get_permissions(self):
        return IsAuthenticated(),

    def post(self, request, pk, format=None):
        serializer = UserEmailSerializer(data=request.data)
        if serializer.is_valid():
            try:
                team = Team.objects.get(pk=pk)
            except ObjectDoesNotExist:
                raise NotFound("Team not found.")
            if request.user in team.accounts.all():
                try:
                    user = User.objects.get(email=serializer.validated_data['email'])
                except ObjectDoesNotExist:
                    raise NotFound("User not found.")
                team.accounts.remove(user)
                return Response({"message": "User removed successfully."})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TeamDetail(APIView):
    def get_permissions(self):
        return IsAuthenticated(),

    def delete(self, request, pk, format=None):
        """
            Removes the requested team.
        """
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")
        if request.user in team.accounts.all():
            team.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        raise PermissionDenied("User is not a team member.")
