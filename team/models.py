from django.db import models
from user.models import User

class Team(models.Model):
    name = models.CharField(max_length=300)
    accounts = models.ManyToManyField(User, blank=True)
    next_sprint_id = models.IntegerField(default=0)

    def backlog(self):
        return self.tasks.filter(sprint=None)

    def get_next_sprint_id(self):
        self.next_sprint_id += 1
        self.save()
        return self.next_sprint_id
