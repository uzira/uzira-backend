from rest_framework import serializers

from .models import Team
from user.serializers import UserSerializer

class TeamSerializer(serializers.ModelSerializer):
    accounts = UserSerializer(many=True, read_only=True)
    class Meta:
        model = Team
        fields = ('id', 'name', 'accounts')
        read_only_fields = ('id', 'accounts')
        depth = 1

class TeamAccountsSerializer(serializers.ModelSerializer):
    accounts = UserSerializer(many=True, read_only=True)
    class Meta:
        model = Team
        fields = ('accounts',)
        depth = 1
