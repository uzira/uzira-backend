from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.exceptions import NotAuthenticated

from .permissions import IsStaffOrTargetUser
from .models import User
from .serializers import AuthTokenSerializer, UserSerializer

class ObtainExpiringAuthToken(ObtainAuthToken):
    serializer_class = AuthTokenSerializer
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data['user']

            Token.objects.filter(user=user).delete()
            token = Token(user=user)
            token.save()

            return Response({'token': token.key})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

obtain_expiring_auth_token = ObtainExpiringAuthToken.as_view()


class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_permissions(self):
        return (AllowAny() if self.request.method == 'POST' else
                IsStaffOrTargetUser()),

    def get_queryset(self):
        """
        This view should allow the user to operate only on themselves if
        the user is not staff.
        """
        if self.request.auth is None:
            raise NotAuthenticated()
        if self.request.user.is_staff:
            return User.objects.all()
        return User.objects.filter(id=self.request.user.id)
