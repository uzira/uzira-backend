from django.urls import path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('', views.UserView, basename='user')

urlpatterns = [
    path('login/', views.obtain_expiring_auth_token),
]

urlpatterns += router.urls