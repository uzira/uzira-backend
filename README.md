# Uzira Backend Project

## Step by step how to configure environment to run backend server

1. Install [Python 3.7+](https://www.python.org/downloads/). Select "Add Python 3.x to PATH".
2. Clone the repository, `cd` into it.
3. Run `python -m venv venv` to create a virtual environment.
4. Run `venv\Scripts\activate.bat` to activate the virtual environment. `(venv)` should appear in the prompt.
5. Run `pip install --upgrade pip` to update pip. If you get an error, try again this time using `python.exe -m pip install --upgrade pip`.
6. Run `pip install -r requirements.txt` to install all the dependencies.

## Applying changes to database after pulling changes from the repository

The commands below should all be run in the repository directory. Also run them before starting the server for
the first time.

1. Run `python manage.py makemigrations` to create the migrations.
2. Run `python manage.py migrate` to apply the migrations.

## Running the server

1. Run `python manage.py runserver` while in the repository directory.

The server is now available at the address given in the console. (default: `127.0.0.1:8000`)

## Adding superuser account

1. Run `python manage.py createsuperuser` to start the process of adding a new superuser.
2. Set the email and the password.
3. The created account can be now used to access the admin panel.

In the admin panel, one can view and modify every existing record, as well as adding new ones.

## Documentation

The documentation can be found [here](https://documenter.getpostman.com/view/5970954/RzfZQt2u).
