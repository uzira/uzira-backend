from django.urls import path
from . import views

sprint_detail = views.SprintDetail.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

task_detail = views.TaskDetail.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

urlpatterns = [
    path('team/<int:pk>/sprints/', views.SprintList.as_view()),
    path('sprint/<int:pk>/', sprint_detail),
    path('team/<int:pk>/backlog/', views.BacklogList.as_view()),
    path('task/<int:pk>/', task_detail),
]