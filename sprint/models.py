from django.db import models
from user.models import User
from team.models import Team


class Sprint(models.Model):
    active = models.BooleanField()
    start_date = models.DateField()
    end_date = models.DateField()
    team = models.ForeignKey(Team, on_delete=models.CASCADE,
                             related_name='sprints')
    inner_id = models.IntegerField()

    def __int__(self):
        return self.id


class Task(models.Model):
    STORY = "story"
    TASK = "task"
    DEFECT = "defect"
    TYPES = (
        (STORY, "Story"),
        (TASK, "Task"),
        (DEFECT, "Defect"),
    )

    STATUSES = (
        ("open", "Open"),
        ("progress", "Progress"),
        ("review", "Review"),
        ("verification", "Verification"),
        ("ready", "Ready"),
    )

    name = models.CharField(max_length=300)
    type = models.CharField(max_length=100, choices=TYPES)
    status = models.CharField(max_length=100, choices=STATUSES, default="open")
    estimation = models.IntegerField()
    assigned = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    description = models.TextField(blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE,
                                    related_name='subtasks', null=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE,
                             related_name='tasks')
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE, null=True,
                               related_name='tasks')

    def __int__(self):
        return self.id
