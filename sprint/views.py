from rest_framework import status, viewsets, mixins, generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied, NotFound
from django.core.exceptions import ObjectDoesNotExist

from team.models import Team
from user.models import User
from .models import Sprint, Task
from .serializers import SprintSerializer, TaskSerializer
from .permissions import IsTeamMember

class SprintList(APIView):
    """
    Create and List sprints.
    """
    def get_permissions(self):
        return IsAuthenticated(),

    def get(self, request, pk, format=None):
        """
        Returns a list of sprints.
        """
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")
        if request.user in team.accounts.all():
            sprints = Sprint.objects.filter(team__id=pk)
            serializer = SprintSerializer(sprints, many=True)
            return Response(serializer.data)
        raise PermissionDenied()

    def post(self, request, pk, format=None):
        """
        Creates a sprint.
        """
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")
        if request.user in team.accounts.all():
            serializer = SprintSerializer(data=request.data)
            if serializer.is_valid():
                sprint = serializer.save(
                    team=team,
                    inner_id=team.get_next_sprint_id()
                )
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        raise PermissionDenied()

class SprintDetail(viewsets.ModelViewSet):
    """
    Sprint Detailed GET, PUT, PATCH and DELETE.
    """
    serializer_class = SprintSerializer
    queryset = Sprint.objects.all()
    permission_classes = [IsAuthenticated, IsTeamMember]


class TaskDetail(viewsets.ModelViewSet):
    """
    Task Detailed GET, PUT, PATCH and DELETE.
    """
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    permission_classes = [IsAuthenticated, IsTeamMember]


class BacklogList(APIView):
    def get_permissions(self):
        return IsAuthenticated() and IsTeamMember(),

    def get(self, request, pk, format=None):
        """
        Return backlog's list of tasks and sprints.
        """
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")
        tasks = team.backlog()
        task_serializer = TaskSerializer(tasks, many=True)

        sprints = team.sprints.all()
        sprint_serializer = SprintSerializer(sprints, many=True)
        return Response({
            "backlog": {
                "tasks": task_serializer.data
            },
            "list": sprint_serializer.data
        })

    def post(self, request, pk, format=None):
        """
        Create a task.
        """
        try:
            team = Team.objects.get(pk=pk)
        except ObjectDoesNotExist:
            raise NotFound("Team not found.")

        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            task = serializer.save(team=team)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
