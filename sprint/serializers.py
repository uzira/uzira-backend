from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import NotFound

from user.models import User
from .models import Task, Sprint

class TaskSerializer(serializers.ModelSerializer):
    parent = serializers.IntegerField(allow_null=True, required=False)
    assigned = serializers.IntegerField(allow_null=True, required=False)
    sprint = serializers.IntegerField(allow_null=True, required=False)
    estimation = serializers.IntegerField(required=False)

    class Meta:
        model = Task
        fields = ('id', 'name', 'type', 'status', 'estimation', 'subtasks',
                'parent', 'assigned', 'sprint', 'description')
        read_only_fields = ('id', 'subtasks')
        depth = 1

    def create(self, validated_data):
        parent_id = validated_data.get('parent', None)
        parent = None
        if parent_id is not None:
            try:
                parent = Task.objects.get(pk=parent_id)
            except ObjectDoesNotExist:
                raise NotFound("Parent not found.")

        assigned_id = validated_data.get('assigned', None)
        assigned = None
        if assigned_id is not None:
            try:
                assigned = User.objects.get(pk=assigned_id)
            except ObjectDoesNotExist:
                raise NotFound("Assigned User not found.")

        sprint_id = validated_data.get('sprint', None)
        sprint = None
        if sprint_id is not None:
            try:
                sprint = Sprint.objects.get(pk=sprint_id)
            except ObjectDoesNotExist:
                raise NotFound("Sprint not found.")

        t = Task(
            name=validated_data.get('name'),
            team=validated_data.get('team'),
            type=validated_data.get('type'),
            status=validated_data.get('status'),
            estimation=validated_data.get('estimation', 0),
            parent=parent,
            assigned=assigned,
            sprint=sprint,
            description=validated_data.get('description', ''),
        )
        t.save()
        return t


    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.type = validated_data.get('type', instance.type)
        instance.status = validated_data.get('status', instance.status)
        instance.estimation = validated_data.get('estimation', instance.estimation)

        parent_id = validated_data.get('parent', None)
        if parent_id is not None:
            try:
                parent = Task.objects.get(pk=parent_id)
            except ObjectDoesNotExist:
                raise NotFound("Parent not found.")
            instance.parent = parent

        assigned_id = validated_data.get('assigned', None)
        if assigned_id is not None:
            try:
                assigned = User.objects.get(pk=assigned_id)
            except ObjectDoesNotExist:
                raise NotFound("Assigned User not found.")
            instance.assigned = assigned

        sprint_id = validated_data.get('sprint', None)
        if sprint_id is not None:
            try:
                sprint = Sprint.objects.get(pk=sprint_id)
            except ObjectDoesNotExist:
                raise NotFound("Sprint not found.")
            instance.sprint = sprint

        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance


class SprintSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)

    class Meta:
        model = Sprint
        fields = ('id', 'inner_id', 'active', 'start_date', 'end_date', 'tasks')
        read_only_fields = ('id', 'inner_id', 'tasks')
        depth = 1
